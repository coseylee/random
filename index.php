<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

// 定义应用名称，配置文件位于本文件同级config目录下，与其同名，后缀为.config(.dev).php
define('APP_NAME', 'demo');

// 引入 Random 核心文件
include 'random/random.php';
//require 'phar://random.phar';

// 初始化
new random();