<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

abstract class random_database
{
	abstract function __construct($db_config);

	abstract function insert($table, $data, $replace = false);
	
	abstract function select($table, $where = '', $order = '', $limit = '', $data = '*');
	
	abstract function select_one($table, $where = '', $order = '', $data = '*');
	
	abstract function update($table, $data, $where);
	
	abstract function delete($table, $where);
	
	abstract function query($sql);
	
	abstract function fetch_array($results);
	
	abstract function fetch_row($results);

	abstract function fetch_next();

	abstract function free_result();

	abstract function num_rows($results);
	
	abstract function affected_rows();

	abstract function insert_id();

	abstract function query_count();
	
	abstract function sqls();
}