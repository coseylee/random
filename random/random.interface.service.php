<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

abstract class random_service
{
	public $error = '';
	public $errno = 0;
	public $data = array();

	/**
	 * 错误处理
	 * 
	 * @param $error 错误消息
	 * @param $errno 错误号
	 * @param $data 返回的数据
	 * @param $isthrow 是否抛出异常
	 * 
	 * @return object
	 */
	function set_error($error, $errno = 0, $data = array(), $isthrow = true)
	{
		$this -> error = $error;
		$this -> errno = $errno;
		$this -> data = $data;
		
		if ($isthrow)
		{
			throw new random_service_exception($this -> error, $this -> errno, $this -> data);
		}
	}
}