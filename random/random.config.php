<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */


$random_config = array();

/* 基础配置 */
$random_config['debug'] = true; // 是否运行于调试模式
$random_config['debug_error_log'] = APP_ROOT.APP_NAME.DIR_SEP.'debug_log.php'; // 关闭调试模式后错误信息文件位置，false关闭
$random_config['php_error_log'] = APP_ROOT.APP_NAME.DIR_SEP.'error_log.php'; // PHP 错误信息文件位置，false关闭
$random_config['error_404'] = false; // 系统错误显示为404，会使得上面debug选项无效，日志会被记录
$random_config['error_404_page'] = ''; // 404 页面
$random_config['gzip'] = false; // 是否自动开启GZIP
$random_config['charset'] = 'utf-8'; // 网站字符集
$random_config['timezone'] = 'Etc/GMT-8'; // 网站时区设置

$random_config['controller_path'] = APP_ROOT.APP_NAME.DIR_SEP.'controller'.DIR_SEP; // 应用控制器目录
$random_config['model_path'] = RANDOM_ROOT.'model'.DIR_SEP; // 数据模型目录
$random_config['service_path'] = RANDOM_ROOT.'service'.DIR_SEP; // 业务模型目录
$random_config['extensions_path'] = RANDOM_ROOT.'extensions'.DIR_SEP; // 第三方扩展目录
$random_config['include_path'][] = APP_ROOT.APP_NAME.DIR_SEP.'include'.DIR_SEP; // 文件包含目录

$random_config['my_controller_file'] = ''; // 控制器基类位置，用于继承 random.interface.controller.php。
$random_config['my_service_file'] = ''; // 业务逻辑基类位置，用于继承 random.interface.service.php。
$random_config['my_route_file'] = ''; // 自定义路由位置，用于替换 random/route 中的模式。

$random_config['onruntime'] = false; // 是否使用 ~runtime.php 文件

/* 路由相关 */
$random_config['route']['mode'] = 'normal'; // normal，rewrite，pathinfo
$random_config['route']['pathinfo'] = false; // pathinfo 时有效，是否开启兼容模式
$random_config['route']['controller_name'] = 'controll'; // 默认控制器参数名
$random_config['route']['controller'] = 'main'; // 默认控制器名
$random_config['route']['action_name'] = 'action'; // 默认动作参数名
$random_config['route']['action'] = 'index'; // 默认动作名
$random_config['route']['rewrite_file'] = '.htaccess'; // 伪静态规则文件名
$random_config['route']['rewrite_separator'] = '-'; // 伪静态分隔符
$random_config['route']['rewrite_suffix'] = '.html'; // 伪静态后缀
$random_config['route']['rewrite_map'] = array(/*'fuckyou' => 'main#demo', 'pagestest' => 'main#pages'*/); // 映射规则 控制器#动作
$random_config['route']['rewrite_args'] = array(/*'pagestest' => array('page')*/); // 隐藏参数名
$random_config['route']['POST'] = array(/*'page' => 1*/);
$random_config['route']['GET'] = array(/*'page' => 1*/);

/* 挂载点 */
$random_config['lanucher']['process_ready'] = array(/*'print_r("aaaaa")')*/); // 初始化已准备好，但并开启进程前
$random_config['lanucher']['process_start'] = array(/*'print_r("aaaaa")')*/); // 已开启进程，准备调用载入控制器前
$random_config['lanucher']['process_end'] = array(/*'print_r("aaaaa")')*/); // 进程已结束，准备销毁内存

/* 缓存 */
$random_config['cache']['type'] = 'file'; // 默认使用的缓存，内置支持 file, sae_file, memcache, sae_memcache, apc
$random_config['cache']['file']['default'] = array('path' => APP_ROOT.APP_NAME.DIR_SEP.'cache'.DIR_SEP);
$random_config['cache']['sae_file']['default'] = array('accesskey' => '', 'secretkey' => '', 'domain' => 'filecache', 'path' => 'cache'.DIR_SEP);
$random_config['cache']['memcache']['deault'] = array('server' => '127.0.0.1', 'port' => '11211');

/* 数据库 */
/*$random_config['db']['default']['driver'] = 'mysqli'; // mysql, mysqli, pdo_mysql, sae_mysql, mongo[,sqlite]
$random_config['db']['default']['name'] = 'random_framework';
$random_config['db']['default']['prefix'] = 'db_';
$random_config['db']['default']['master'][] = array('host' => 'localhost', 'port' => 3306, 'user' => 'root', 'pass' => '');
$random_config['db']['default']['slave'][] = array('host' => 'localhost', 'port' => 3306, 'user' => 'root', 'pass' => '');*/

/* 模板 */
$random_config['template']['template_dir'] = APP_ROOT.APP_NAME.DIR_SEP.'template'.DIR_SEP; // 模版源文件目录
$random_config['template']['cache_dir'] = APP_ROOT.APP_NAME.DIR_SEP.'template'.DIR_SEP.'cache'.DIR_SEP; // 模版缓存目录
$random_config['template']['separator'] = '_'; // 模版分隔符，"DIR_SEP" 为目录