<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

class normal_route extends random_route
{
	function url_parse()
	{
		return true;
	}
	
	function url_create($controller = '', $action = '', $args = array(), $query_string = '')
	{
		$config = $GLOBALS['RANDOM']['route'];
		$url = $config['url_base'];
		
		if ($controller === '' && $action === '' && empty($args))
		{
			return $url;
		}
		
		$query = array();
		
		if ($controller !== '')
		{
			$query[$config['controller_name']] = $controller;
		}
		
		if ($action !== '')
		{
			$query[$config['action_name']] = $action;
		}
		
		foreach ($args as $k => $v)
		{
			$query[$k] = $v;
		}

		if (!empty($query))
		{
			$url .= '?'.http_build_query($query);
		}
		
		return $url;
	}
}