<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

abstract class random_model
{
	// 表名
	protected $table_name = '';
	// 表前缀
	protected $table_pre = '';
	// 完全表名
	protected $table = '';
	// 数据库配置名称
	protected $db_config = '';
	
	function __construct()
	{
		$this -> db_config = DB::connectAdd($this -> db_config);
		if (isset($GLOBALS['RANDOM']['db'][ $this -> db_config ]['prefix']))
		{
			$this -> table_pre = $GLOBALS['RANDOM']['db'][ $this -> db_config ]['prefix'];
		}
		$this -> table = $this -> table_pre.$this -> table_name;
	}
	
	function insert($data, $replace = false)
	{
		return DB::insert($this -> table, $data, $replace, $this -> db_config);
	}
	
	function select($where = '', $order = '', $limit = '', $data = '*')
	{
		return DB::select($this -> table, $where, $order, $limit, $data, $this -> db_config);
	}
	
	function select_one($where = '', $order = '', $data = '*')
	{
		return DB::select_one($this -> table, $where, $order, $data, $this -> db_config);
	}
	
	function update($data, $where)
	{
		return DB::update($this -> table, $data, $where, $this -> db_config);
	}
	
	function delete($where)
	{
		return DB::delete($this -> table, $where, $this -> db_config);
	}
	
	function query($sql, $format = true)
	{
		return DB::query($sql, $format, $this -> db_config);
	}
	
	function start()
	{
		return DB::start($this -> db_config);
	}
	
	function commit()
	{
		return DB::commit($this -> db_config);
	}
	
	function rollback()
	{
		return DB::rollback($this -> db_config);
	}

	function pager($curpage = 1, $perpage = 10)
	{
		return DB::pager($curpage, $perpage, $this -> table, $this -> db_config);
	}
	
	function num_rows($results = null)
	{
		return DB::num_rows($results);
	}
	
	function affected_rows()
	{
		return DB::affected_rows();
	}

	function insert_id()
	{
		return DB::insert_id();
	}
	
	function return_msg($status, $msg = '', $data = array())
	{
		return array($status, $msg, $data);
	}
}