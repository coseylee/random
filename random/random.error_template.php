<html>
<head>
<title><?php echo strip_tags($this -> message); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	background-color:#fff;
	margin:20px 40px;
	font-family:Lucida Grande, Verdana, Sans-serif;
	font-size:12px;
	color:#000;
}
#debug_content {
	border:#999 1px solid;
	background-color:#fff;
	padding:20px 20px 12px 20px;
}
#debug_time {
	padding:20px 20px 12px 0px;
}
h1 {
	font-weight:bold;
	font-size:16px;
	line-height:26px;
	color:#990000;
	margin:0 0 20px 0;
}
</style>
</head>
<body>
	<div id="debug_content">
		<h1><?php echo $this -> message; ?></h1>
		<?php if(is_array($debug) && $this -> show_trace == true) foreach ($debug as $line): ?>
		<p><?php echo $line;?></p>
		<?php endforeach; ?>
	</div>
	<?php if ($this -> show_trace == true): ?>
	<div id="debug_time">
		Processed in <?php echo $debug_time; ?> second(s).
	</div>
	<?php endif; ?>
</body>
</html>
