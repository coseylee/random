<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

class cache_memcache
{
	private $memcache = null;
	
	function __construct($config)
	{
		if (!extension_loaded('memcache'))
		{
			throw new random_exception('This Server Dont Load Memcahe Extension.');
		}
		
		if(!empty($config['server']) && $this -> memcache == null)
		{
			$this -> memcache = new Memcache;
			$conn = $this -> memcache -> connect($config['server'], $config['port']);
			if ($conn === false)
			{
				throw new random_exception('Cant Connect To Memcache Server!');
			}
		}
	}
	
	function set($name, $data, $ttl)
	{
		return $this -> memcache -> set($name, $data, MEMCACHE_COMPRESSED, $ttl);
	}
	
	function get($name)
	{
		return $this -> memcache -> get($name);
	}
	
	function delete($name)
	{
		return $this -> memcache -> delete($name);
	}
	
	function clear()
	{
		return $this -> memcache -> flush();
	}
}