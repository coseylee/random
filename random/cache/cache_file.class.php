<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

class cache_file
{
	private $cache_path = '';
	private $cache_pre = 'cache_';
	private $cache_ext = '.php';
	
	function __construct($config)
	{
		$this -> cache_path = $config['path'];
		if (!is_dir($this -> cache_path))
		{
			mkdir($this -> cache_path, 0777, true);
			file_put_contents($this -> cache_path.'index.html', '');
		}
	}
	
	function set($name, $data, $ttl)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		$data = '<?php'.PHP_EOL.'/* Created At '.date('Y-m-d H:i:s').' */'.PHP_EOL.'return '.str_replace('  ', "\t", var_export($data, true)).';'.PHP_EOL;
		$return = file_put_contents($cachefile, $data, LOCK_EX);
		return $return;
	}
	
	function get($name)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		if (!file_exists($cachefile))
		{
			return false;
		}
		$data = file_get_contents($cachefile);
		if(stripos($data, '<?php') !== false)
		{
			$data = str_replace(array('<?php', '?>'), '', $data);
			$return = eval($data.';');
			return $return;
		}
		return null;
	}
	
	function delete($name)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		if (file_exists($cachefile))
		{
			@unlink($cachefile);
		}
		return true;
	}
	
	function clear()
	{
		return true;
	}
}