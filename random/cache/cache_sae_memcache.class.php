<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

/**
 * 新浪云计算 MemcacheX 操作类
 */
class cache_sae_memcache
{
	private $memcache = null;
	
	function __construct()
	{
		if (!function_exists('memcache_init'))
		{
			throw new random_exception('SaeMemcache Only For Sina App Engine.');
		}

		if($this -> memcache == null)
		{
			$this -> memcache = memcache_init();
			if ($this -> memcache === false)
			{
				throw new random_exception('Cant Connect To MemcacheX Server!');
			}
		}
	}
	
	function set($name, $data, $ttl)
	{
		return $this -> memcache -> set($name, $data, MEMCACHE_COMPRESSED, $ttl);
	}
	
	function get($name)
	{
		return $this -> memcache -> get($name);
	}
	
	function delete($name)
	{
		return $this -> memcache -> delete($name);
	}
	
	function clear()
	{
		return $this -> memcache -> flush();
	}
}