<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

/**
 * 新浪云计算 基于 Storage 文件缓存 操作类
 */
class cache_sae_file
{
	private $storage = null;
	private $storage_domain = '';
	private $cache_path = '';
	private $cache_pre = 'cache_';
	private $cache_ext = '.php';
	
	function __construct($config)
	{
		if (!class_exists('SaeStorage'))
		{
			throw new random_exception('SaeStorage Only For Sina App Engine.');
		}
		
		if ($this -> storage == null)
		{
			$this -> storage = new SaeStorage($config['accesskey'], $config['secretkey']);
		}
		
		$this -> storage_domain = $config['domain'];
		$this -> cache_path = $config['path'];
		
		// 发现云计算平台很强，下面基本没用 - -
		if (!$this -> storage -> fileExists($this -> storage_domain, 'index.html'))
		{
			$this -> storage -> write($this -> storage_domain, 'index.html', '');
		}
		
	}
	
	function set($name, $data, $ttl)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		$data = '<?php'.PHP_EOL.'/* Created At '.date('Y-m-d H:i:s').' */'.PHP_EOL.'return '.str_replace('  ', "\t", var_export($data, true)).';'.PHP_EOL;
		$return = $this -> storage -> write($this -> storage_domain, $cachefile, $data);
		return $return;
	}
	
	function get($name)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		if (!$this -> storage -> fileExists($this -> storage_domain, $cachefile))
		{
			return false;
		}
		$data = $this -> storage -> read($this -> storage_domain, $cachefile);
		if(stripos($data, '<?php') !== false)
		{
			$data = str_replace(array('<?php', '?>'), '', $data);
			$return = eval($data.';');
			return $return;
		}
		return null;
	}
	
	function delete($name)
	{
		$cachefile = $this -> cache_path.$this -> cache_pre.$name.$this -> cache_ext;
		if ($this -> storage -> fileExists($this -> storage_domain, $cachefile))
		{
			@$this -> storage -> delete($this -> storage_domain, $cachefile);
		}
		return true;
	}
	
	function clear()
	{
		return true;
	}
}