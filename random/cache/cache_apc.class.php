<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

class cache_apc
{
	function __construct($config)
	{
		if (!function_exists('apc_fetch'))
		{
			throw new random_exception('This Server Dont Load APC Extension.');
		}
	}
	
	function set($name, $data, $ttl)
	{
		return apc_store($name, $data, $ttl);
	}
	
	function get($name)
	{
		return apc_fetch($name);
	}
	
	function delete($name)
	{
		return apc_delete($name);
	}
	
	function clear()
	{
		return apc_clear_cache('user');
	}
}
