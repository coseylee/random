<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

abstract class random_route
{
	// 预处理
	final function __construct()
	{
		// ZEND FRAMEWORK
		if(basename($_SERVER['SCRIPT_NAME']) === basename($_SERVER['SCRIPT_FILENAME']))
		{
			$GLOBALS['RANDOM']['route']['url_base'] = $_SERVER['SCRIPT_NAME'];
		}
		elseif (basename($_SERVER['PHP_SELF']) === basename($_SERVER['SCRIPT_FILENAME']))
		{
			$GLOBALS['RANDOM']['route']['url_base'] = $_SERVER['PHP_SELF'];
		}
		elseif (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === basename($_SERVER['SCRIPT_FILENAME']))
		{
			$GLOBALS['RANDOM']['route']['url_base'] = $_SERVER['ORIG_SCRIPT_NAME'];
		}
		
		$this -> config = $GLOBALS['RANDOM']['route'];
		
		$this -> url_parse();
		return $this -> prepare_process();
	}
	
	// url 解析方法
	abstract function url_parse();
	
	// url 生成方法
	abstract function url_create($controller = '', $action = '', $args = array(), $query_string = '');
	
	// 生成控制器及动作常量
	final private function prepare_process()
	{
		// 附加参数
		$_GET = array_merge($this -> config['GET'], $_GET);
		$_POST = array_merge($this -> config['POST'], $_POST);
		
		// 控制器名称
		$GLOBALS['RANDOM']['CONTROLLER'] = $controller = $_GET[$this -> config['controller_name']] = isset($_GET[$this -> config['controller_name']]) ? trim($_GET[$this -> config['controller_name']]) : (isset($_POST[$this -> config['controller_name']]) ? trim($_POST[$this -> config['controller_name']]) : $this -> config['controller']);
		
		// 动作名称
		$GLOBALS['RANDOM']['ACTION'] = $action = $_GET[$this -> config['action_name']] = isset($_GET[$this -> config['action_name']]) ? trim($_GET[$this -> config['action_name']]) : (isset($_POST[$this -> config['action_name']]) ? trim($_POST[$this -> config['action_name']]) : $this -> config['action']);

		// 子目录
		$sub_dir = $GLOBALS['RANDOM']['route']['controller_sub_dir'];
		$GLOBALS['RANDOM']['route']['controller_sub_dir'] = array_filter(explode(DIR_SEP, $sub_dir));

		define('CONTORLLER_SUB_DIR', $sub_dir);
		define('CONTROLLER', $controller);
		define('ACTION', $action);
	}
}