<?php
/**
 * [ Random Framework ]
 *
 * @version 2.0.0 (build 20130520)
 *
 * @link http://0x00.sinaapp.com/random.html
 * 
 * @author Coseylee (Coseylee@gmail.com).
 *
 */

final class CACHE
{
	static private $cache = null;
	static private $pool = array();

	/**
	 * 获得缓存实例
	 * 
	 * @param $type 缓存类型
	 * @param $name 缓存名称
	 * 
	 * @return object
	 */
	public static function getInstance($type, $name)
	{
		if (isset(self::$pool[ $type ][ $name ]))
		{
			return self::$pool[ $type ][ $name ];
		}
		else
		{
			$cache_class = 'cache_'.$type;
			if (random::import(RANDOM_ROOT.'cache'.DIR_SEP.$cache_class.'.class.php', false) || random::import($cache_class.'.class.php', false))
			{
				$config = array();
				if (isset($GLOBALS['RANDOM']['cache'][ $type ][ $name ]))
				{
					$config = $GLOBALS['RANDOM']['cache'][ $type ][ $name ];
				}
				self::$pool[ $type ][ $name ] = random::newInstance($cache_class, array($config));
				return self::$pool[ $type ][ $name ];
			}
		}
		return false;
	}

	public static function init()
	{
		if (!empty($GLOBALS['RANDOM']['cache']['type']) && self::$cache == null)
		{
			self::$cache = self::getInstance($GLOBALS['RANDOM']['cache']['type'], 'default');
		}
		return true;
	}
	
	public static function set($name, $data, $ttl = 0)
	{
		return self::$cache -> set($name, $data, $ttl);
	}
	
	public static function get($name)
	{
		return self::$cache -> get($name);
	}
	
	public static function delete($name)
	{
		return self::$cache -> delete($name);
	}
	
	public static function clear()
	{
		return self::$cache -> clear();
	}
}