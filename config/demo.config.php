<?php
$app_config = array();

/* 基础配置 */
$app_config['debug'] = true;
$app_config['model_path'] = APP_ROOT.APP_NAME.DIR_SEP.'model'.DIR_SEP;
$app_config['service_path'] = APP_ROOT.APP_NAME.DIR_SEP.'service'.DIR_SEP;
$app_config['gzip'] = true;

/* 路由相关 */
$app_config['route']['mode'] = 'pathinfo';

/* 数据库 */
$app_config['db']['default']['driver'] = 'mysqli'; // mysql, mysqli, pdo_mysql, sae_mysql, mongo[,sqlite]
$app_config['db']['default']['name'] = 'random_framework';
$app_config['db']['default']['prefix'] = 'db_';
$app_config['db']['default']['master'][] = array('host' => 'localhost', 'port' => 3306, 'user' => 'root', 'pass' => '');

$app_config['db']['modeldb']['driver'] = 'mysql'; // mysql, mysqli, pdo_mysql, sae_mysql, mongo[,sqlite]
$app_config['db']['modeldb']['name'] = '^_^';
$app_config['db']['modeldb']['prefix'] = '';
$app_config['db']['modeldb']['master'][] = array('host' => 'localhost', 'port' => 3306, 'user' => 'root', 'pass' => '');