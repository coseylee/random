<?php
$phar = new Phar('random.phar', 0);
$phar -> buildFromDirectory(dirname(__FILE__).'/random');

$stub = <<<ENDSTUB
<?php
Phar::mapPhar('random.phar');
require 'phar://random.phar/random.php';
__HALT_COMPILER();
ENDSTUB;

$phar -> setStub($stub);
$phar -> compressFiles(Phar::GZ);
$phar -> stopBuffering();