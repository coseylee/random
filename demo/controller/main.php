<?php
/**
 * 默认演示文件
 */

class main extends random_controller
{
	function index()
	{
		echo 'Welcome To Random PHP Framework. Version:'.random::VERSION.' '.random::RELEASE.'<br/><hr>';
		dump($_GET);
		echo '数据库测试<hr>';
		$data = DB::select_one('random_admin_log', array('logid' => '3'));
		dump($data);
		//DB::insert('random_admin_log', array('logid' => 2));
		
		echo '分页查询<hr>';
		dump(DB::pager(5, 10) -> query('SELECT * FROM (SELECT * FROM `db_admin_log`) a'));
		dump(DB::pager(5, 10) -> getPage());

		dump(DB::pager(1, 2) -> select('db_admin_log', 'logid < 10', '', '', '*'));
		dump(DB::pager(1, 2) -> getPage());
		
		echo '查询语句<hr>';
		dump(DB::query('SELECT * FROM `db_admin_log` LIMIT 3'));
		dump(DB::sqls());
		
		echo '地址生成(异常处理)<hr>';
		try
		{
			dump(random::service('demo') -> create_url(100));
			dump(random::service('demo', true) -> create_url(-1));
		}
		catch (Exception $e)
		{
			echo $e;
			dump($e -> getMessage());
			dump($e -> getCode());
			dump($e -> getData());
		}

		echo '当前页地址<hr>';
		dump(thisURL(true));
		
		echo '配置信息<hr>';
		dump($GLOBALS['RANDOM']);
	}
	
	function model()
	{
		$data = random::model('admin_log') -> select('', 'logid DESC, time ASC', '3');
		dump($data);
		
		echo '模型分页查询 QUERY() <hr>';
		dump(random::model('admin_log') -> pager(6, 3) -> query('SELECT * FROM (SELECT * FROM `random_admin_log`) a'));
		dump(random::model('admin_log') -> pager() -> getPage());
		
		echo '模型分页查询 SELECT() <hr>';
		dump(random::model('admin_log') -> pager(1, 2) -> select('logid < 10', 'logid DESC', '', '*'));
		dump(random::model('admin_log') -> pager(1, 2) -> getPage());
		
		echo '原生分页查询<hr>';
		dump(DB::pager(1, 2) -> select('db_admin_log', 'logid < 10', '', '', '*'));
		
		echo '模型跨库<hr>';
		$model = random::model('ipdata');
		$data = $model -> select('', '', '0, 3');
		dump($data);

		echo '模型跨库分页 SELECT() <hr>';
		dump($model -> pager(5000, 5) -> select());
		dump($model -> pager(5000, 5) -> getPage());
		
		echo '模型跨库分页 QUERY() <hr>';
		dump($model -> pager(5000, 5) -> query('SELECT * FROM `ipdata`'));
		dump($model -> pager(5000, 5) -> getPage());
		
		echo '所有查询语句<hr>';
		dump(DB::sqls());
		echo '配置信息<hr>';
		dump($GLOBALS['RANDOM']);
	}
	
	function template()
	{
		$abc = 'TMD';
		$data = CACHE::get('telphone');
		//showMessage('跳转提示测试文字', url(CONTROLLER, 'demo', array('page' => 90, 'contentid' => 109)), '', 10);
		include template('demo');
	}
	
	function cache()
	{
		echo '缓存演示，支持文件、APC、Memcached及新浪云平台<hr>';
		$data = array('China Unicom' => '10010', 'China Mobile' => '10086', 'China Telcom' => '10000');
		//dump(CACHE::set('telphone', $data));
		dump(CACHE::get('telphone'));
		echo debug_time();
		//CACHE::getInstance('memcache', 'default');
	}
	
	function pages()
	{
		echo <<<EOT
		<style>
		.pages {
			padding-top:10px;
			padding-right:5px;
			height:30px;
			clear:both;
			text-align:right;
		}
		.pages a {
			border:1px solid #E3E3E3;
			padding:5px 9px;
			margin:0 1px;
		}
		.pages a:hover {
			background:#F5F5F5;
			text-decoration:none;
		}
		.pages a.cur {
			font-weight:bold;
			background:#F0F0F0;
			text-decoration:none;
		}
		</style>
EOT;
		$_GET['page'] = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$model = random::model('ipdata');
		echo '模型跨库分页实例演示<hr>';
		dump($model -> pager($_GET['page'], 10) -> select());
		$page = $model -> pager($_GET['page'], 10) -> getPage();
		echo showPageLinks($page['total_size'], $page['page_size'], $_GET['page']);
	}
	
	function test()
	{
		$model = random::model('ipdata');
		$data = $model -> start();
		$data = array(
			'startip' => '0000',
			'endip' => '1111',
			'country' => 'XXXXX',
			'local' => 'AAAA'
		);
		$data = $model -> insert($data);
		//$data = $model -> commit();
		//$data = $model -> rollback();
		var_dump($data);
	}
	
	function socket()
	{
		random::import('sockopen_url.php');
		$http = new sockopen_url();
		$http -> setHost('0x00.sinaapp.com');
		$response = $http -> _UPLOAD('/', array('a' => 'b'), array(array('input' => 'input', 'name' => 'aaaaa', 'filedata' => 'ddddddd')), 3);
		if ($response !== false)
		{
			dump($response);
		}
		else
		{
			dump($http -> getError());
		}
	}
	
	function mongo()
	{
		for ($i = 0; $i < 5; $i++)
		{
			//dump(DB::insert('test', array('uid' => 'Name_'.$i, 'password' => md5(time()))));
		}
		
		//$obj = DB::insert('test', array('uid' => 999, 'password' => md5(time())));
		
		dump(DB::select('test', '', array('uid' => -1), array(0, 10)));
	
		//dump(DB::select_one('test', '', array('uid' => -1)));
		
		//dump(DB::delete('test', array()));
		
		//var_dump(DB::update('test', array('$set' => array('password' => '888888888888888')), array('uid' => 2)));
		//var_dump(DB::update('test', array('password' => '123456', 'uid' => 2), array('password' => '123456')));
		echo '查询次数：<br />';
		dump(DB::query_count());
		echo '查询情况：<br />';
		dump(DB::sqls());
		dump($GLOBALS['RANDOM']);
	}
}