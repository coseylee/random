<?php

class demo_service extends random_service
{
	function create_url($contentid)
	{
		if ($contentid < 0)
		{
			$this -> set_error('contentid must be greater than 0', 1, array());
			return false;
		}
		return url('main', 'demo', array('page' => 3, 'contentid' => $contentid));
	}
}